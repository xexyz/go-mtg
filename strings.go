package mtg

import (
    "bytes"
    "fmt"
    "reflect"
)

func Stringify(message interface{}) string {
    var buf bytes.Buffer
    v := reflect.ValueOf(message)
    stringifyValue(&buf, v)
    return buf.String()
}

func stringifyValue(buf *bytes.Buffer, val reflect.Value) {
    if val.Kind() == reflect.Ptr && val.IsNil() {
        buf.WriteString("<nil>")
        return
    }

    v := reflect.Indirect(val)

    switch v.Kind() {
    case reflect.String:
        fmt.Fprintf(buf, `"%s"`, v)
    case reflect.Slice:
        buf.WriteString("\n[\n")
        for i := 0; i < v.Len(); i++ {
            if i > 0 {
                buf.WriteByte(' ')
            }

            stringifyValue(buf, v.Index(i))
        }

        buf.WriteString("\n]\n")
        return
    case reflect.Struct:
        if v.Type().Name() != "" {
            buf.WriteString(v.Type().String())
        }

        buf.WriteString("\n{")

        var sep bool
        for i := 0; i < v.NumField(); i++ {
            fv := v.Field(i)
            if fv.Kind() == reflect.Ptr && fv.IsNil() {
                continue
            }
            if fv.Kind() == reflect.Slice && fv.IsNil() {
                continue
            }

            if sep {
                buf.WriteString(", ")
            } else {
                sep = true
            }

            buf.WriteString(v.Type().Field(i).Name)
            buf.WriteByte(':')
            stringifyValue(buf, fv)
        }

        buf.WriteString("}\n")
    default:
        if v.CanInterface() {
            fmt.Fprint(buf, v.Interface())
        }
    }
}

