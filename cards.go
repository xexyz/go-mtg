package mtg

import (
	"fmt"
)

type CardsService service


type CardsRoot struct{
	Cards             []*Card      `json:"cards"` 
}

type CardRoot struct{
	Card               *Card      `json:"card"` 
}

type Card struct {
	ID                  string            `json:"id"`
    Name                string            `json:"name"`
	Names               []string          `json:"names"`
	ManaCost            string            `json:"manaCost"`
	ConvertedManaCost   int               `json:"cmc"`
	Colors              []string          `json:"colors"`
	ColorIdentity       []string            `json:"colorIdentity"`
	Type                string            `json:"type"`
	Supertypes          []string          `json:"supertypes"`
	Types               []string          `json:"types"`
    Subtypes            []string          `json:"subtypes"`
	Rarity              string            `json:"rarity"`
	Set                 string            `json:"set"`
	Text                string            `json:"text"`
	Artist              string            `json:"artist"`
	Number              string            `json:"number"`
    Power               string            `json:"power"`
	Toughness           string            `json:"toughness"`
	Layout              string            `json:"layout"`
	MultiverseID        int               `json:"multiverseid"`
	ImageURL            string            `json:"imageUrl"`
	Rulings             []*Ruling         `json:"rulings"`
    ForeignNames        []*ForeignName    `json:"foreignNames"`
	Printings           []string          `json:"printings"`
	OriginalText        string            `json:"originalText"`
	OriginalType        string            `json:"originalType"`
}

type Ruling struct {
	Date      string      `json:"date"`
	Text      string      `json:"text"`
}

type ForeignName struct {
	Name                string      `json:"name"`
	Language            string      `json:"language"`
	MultiverseID        int         `json:"multiverseid"`
}

func (s Card) String() string {
	return Stringify(s)
}


type GetCardsOptions struct {
	Limit             int        `url:"limit,omitempty"`
	StartDate         string     `url:"start_date,omitempty"`
	EndDate           string     `url:"end_date,omitempty"`
}


func (s *CardsService) GetCards(opt *GetCardsOptions) ([]*Card, *Response, error) {
	u := fmt.Sprintf("cards")
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CardsRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.Cards, resp, err
}

func (s *CardsService) GetCard(multiverseId int, opt *GetCardsOptions) (*Card, *Response, error) {
	u := fmt.Sprintf("cards/%d", multiverseId)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *CardRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.Card, resp, err
}
