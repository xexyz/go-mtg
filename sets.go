package mtg

import (
	"fmt"
)

type SetsService service


type SetsRoot struct{
	Sets             []*Set      `json:"sets"` 
}

type SetRoot struct{
	Set               *Set      `json:"set"` 
}

type Set struct {
	Code                string            `json:"code"`
	Border              string            `json:"border"`
	MKMID               int               `json:"mkm_id"`
    Name                string            `json:"name"`
	Booster             []interface{}     `json:"booster"`
	MKM_Name            string            `json:"mkm_name"`
	ReleaseDate         string            `json:"releaseDate"`
	MagicCardsInfoCode  string            `json:"magicCardsInfoCode"`
	Block               string            `json:"block"`
}


func (s Set) String() string {
	return Stringify(s)
}


type GetSetsOptions struct {
	Limit             int        `url:"limit,omitempty"`
	StartDate         string     `url:"start_date,omitempty"`
	EndDate           string     `url:"end_date,omitempty"`
}


func (s *SetsService) GetSets(opt *GetSetsOptions) ([]*Set, *Response, error) {
	u := fmt.Sprintf("sets")
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *SetsRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.Sets, resp, err
}

func (s *SetsService) GetSet(code string, opt *GetSetsOptions) (*Set, *Response, error) {
	u := fmt.Sprintf("sets/%s", code)
	req, err := s.client.NewRequest("GET", u, opt)
	if err != nil {
		return nil, nil, err
	}

	var c *SetRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.Set, resp, err
}

func (s *SetsService) GenBooster(code string) ([]*Card, *Response, error) {
	u := fmt.Sprintf("sets/%s/booster", code)
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c *CardsRoot
	resp, err := s.client.Do(req, &c)
	
	if err != nil {
		return nil, resp, err
	}

	return c.Cards, resp, err
}
