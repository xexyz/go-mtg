package mtg

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"

	"github.com/google/go-querystring/query"
)

const (

	libraryVersion = "1"
	defaultBaseURL = "https://api.magicthegathering.io/v1/"
	userAgent      = "go-mtg" + libraryVersion
	apiDocs        = "https://docs.magicthegathering.io/"
)

type Client struct {
	client *http.Client

	baseURL *url.URL
	UserAgent string

	Cards              *CardsService
	Sets               *SetsService
}

type service struct {
        client *Client
}

type Response struct {
	*http.Response
}

type ErrorResponse struct {
	Response *http.Response
	Message  string
}

func NewClient(httpClient *http.Client) *Client{
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	
	c:= &Client{client: httpClient, UserAgent: userAgent}
	if err := c.SetBaseURL(defaultBaseURL); err != nil {
		panic(err)
	}

	// Services
	c.Cards = &CardsService{client: c}
	c.Sets = &SetsService{client: c}

	return c
}

func (c *Client) BaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

func (c *Client) SetBaseURL(urlStr string) error {
	// Make sure the given URL end with a slash
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	var err error
	c.baseURL, err = url.Parse(urlStr)
	return err
}

func (c *Client) NewRequest(method, path string, opt interface{}) (*http.Request, error) {
	u := *c.baseURL
	u.Opaque = c.baseURL.Path + path

	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		u.RawQuery = q.Encode()
	}

	req := &http.Request{
		Method:         method,
		URL:            &u,
		Proto:          "HTTP/1.1",
		ProtoMajor:     1,
		ProtoMinor:     1,
		Header:         make(http.Header),
		Host:           u.Host,
	}

	req.Header.Set("Accept", "application/json")

	if c.UserAgent != "" {
		req.Header.Set("User-Agent", c.UserAgent)
	}

	return req, nil
}


func newResponse(r *http.Response) *Response {
	response := &Response{Response: r}
	return response
}

func (c *Client) Do (req *http.Request, v interface{}) (*Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	response := newResponse(resp)
	err = CheckResponse(resp)

	if err != nil {
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return response, err
}


func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s", e.Response.Request.Method, u, e.Response.StatusCode, e.Message)
}

// CheckResponse checks the API response for errors, and returns them if present.
func CheckResponse(r *http.Response) error {
	switch r.StatusCode {
	case 200, 201, 304:
		return nil
	}

	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && data != nil {
		var raw interface{}
		if err := json.Unmarshal(data, &raw); err != nil {
			errorResponse.Message = "failed to parse unknown error format"
		}

		errorResponse.Message = parseError(raw)
	}

	return errorResponse
}

func parseError(raw interface{}) string {
	switch raw := raw.(type) {
	case string:
		return raw

	case []interface{}:
		var errs []string
		for _, v := range raw {
			errs = append(errs, parseError(v))
		}
		return fmt.Sprintf("[%s]", strings.Join(errs, ", "))

	case map[string]interface{}:
		var errs []string
		for k, v := range raw {
			errs = append(errs, fmt.Sprintf("{%s: %s}", k, parseError(v)))
		}
		sort.Strings(errs)
		return strings.Join(errs, ", ")

	default:
		return fmt.Sprintf("failed to parse unexpected error type: %T", raw)
	}
}

func String(v string) *string {
	p := new(string)
	*p = v
	return p
}


// // to store v and returns a pointer to it.
// func Bool(v bool) *bool {
// 	p := new(bool)
// 	*p = v
// 	return p
// }

// // Int is a helper routine that allocates a new int32 value
// // to store v and returns a pointer to it, but unlike Int32
// // its argument value is an int.
// func Int(v int) *int {
// 	p := new(int)
// 	*p = v
// 	return p
// }

